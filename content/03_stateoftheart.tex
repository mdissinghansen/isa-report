\newpage
\newsection{State-of-the-art}\label{stateoftheart}
%Describe state-of-the-art (how is the main issue addressed today?) related to the main issue and enough of its underpinning theory in this section, such that you later can explain why and how your system provides a solution to the essential problems related to the main issue.

%Provide an overview of existing systems, if any exist. What are their key features and why are they not sufficient? What are their shortcomings? On what grounds do you make your conclusions?

\subsection{Bayesian Hit Selection, \citeauthor{Zhang2008}}\label{bayesian}
The algorithm used for Bayesian hit selection was implemented, because it provides false discovery rate control to address multiple testing issues, and because it is more robust to outliers than comparable methods, such as median $\pm$kMAD \citep{Birmingham2010}. \citeauthor{Zhang2008} suggest two methods for creating Bayesian models for RNAi interference screening. Only one of the two methods (Model 1) was chosen for implementation, since Model 1 was simpler, made less assumptions, and yielded good or better results than the more complex Model 2. The algorithm was implemented in R, with variations, as explained below:\\

The input of this algorithm is a data file, containing values, which for testing purposes were Cell Titer Blue (CTB) values. CTB is a metabolic readout reflecting cell viability and therefore a standard choice for high-throughput screening experiments. The additional parameters were cutoff for the inhibitor and activator values.

Firstly, the intensities (CTB values) were log transformed into $X_1 ,X_2 ,...,X_k$. The likelihood function is assumed to follow a standard distribution as in (\ref{likelihood}). Where $\mu_i$ is the mean for the sample value $i$. The hypotheses of interest are given by (\ref{hyps}), for which probabilities will be calculated.

\begin{equation}\label{likelihood}
X_i | \mu_i \sim N(\mu_i , \sigma^2)
\end{equation}

\begin{eqnarray}\label{hyps}
H_0&:&\text{CTB}\hspace*{0.2cm} i\in\{\text{no effect}\}\\
H_1&:&\text{CTB}\hspace*{0.2cm} i\in\{\text{activation effect}\}\nonumber\\
H_2&:&\text{CTB}\hspace*{0.2cm} i\in\{\text{inhibitor effect}\}\nonumber
\end{eqnarray}

Next, the log transformed CTB values of the negative control plates, $Y_{ij}$, were found, where $i$ is the negative control number, and $j$ is the plate number. The platewise mean of the negative control values, $Y_{\bullet j}$ were also calculated from log transformed CTB values. So $Y_{\bullet j}$ is the mean of the negative control values in plate $j$.

The difference of value from mean is calculated, as shown in (\ref{diffvalmean}). In \citep{Zhang2008}, a box plot rule is applied for excluding outliers. However, this step was omitted here, because of the limited number of available controls in the 96-well format used in the screens at the NanoCAN center.


\begin{equation}\label{diffvalmean}
d_{ij} = Y_{ij} - Y_{\bullet j}\hspace*{0.4cm} \forall i, \forall j
\end{equation}

The estimate of variance $\widehat{\sigma}^2$, is given by (\ref{estvar}), where $n_j$ is the amount of control well plates of plate $j$, and $A$ is the total amount of plates.

\begin{equation}\label{estvar}
\widehat{\sigma}^2 = \frac{\sum_i\sum_j d_{ij}^2}{\sum_j n_j - A}
\end{equation}

The variance, $Var(X_i)$, is calculated in (\ref{varx}), and here it was assumed that $\widehat{\sigma}^2=\sigma^2$, because of the small sample size. Because Model 1 was used, the equations (\ref{varx}) and (\ref{tau2}) were significantly simplified. Because of this simplification, the resulting $Var(X_i)$ will be the same for all $X_i$.

\begin{eqnarray}
Var(X_i) &=& \widehat{\sigma}^2 + \tau^2 \sum p_{m}^2 = \sigma^2 + \tau^2 \label{varx}\\
\tau^2 &=& max \left[\frac{V\widehat{a}r(X)-\widehat{\sigma}^2}{\sum p_{m}^2} ,0 \right]\label{tau2}\\
\sum p_{m}^2 &=& 1 \hspace*{0.4cm} \text{(Because Model 1 was used)}
\end{eqnarray}

Second to last, the prior and posterior distributions are calculated. Let the prior distribution of $\mu_i$ be $\pi(\mu_i)$, and $\mu_i$ have the distribution (\ref{priordist}). The prior mean of CTB value $i$, is set to be the value of $Y_{\bullet w}$, where $w$ is the plate number of the given CTB $i$. The prior variance is $\tau^2$. The posterior distribution is given by (\ref{postdist}). $\theta_0$ is here given by $Y_{\bullet w}$, and thus all the values needed to do the bayesian hypothesis testing, are found.

\begin{eqnarray}
\mu_i &\sim& N(\theta_0 , \sigma^2)\label{priordist}\\
\mu_i | X_i &\sim& N\left( \frac{ \sigma^2 \theta_0 + \tau^2 X_i}{\sigma^2+\tau^2}, \frac{\sigma^2\tau^2}{\sigma^2+\tau2} \right)\label{postdist} \\
 & \Downarrow & \nonumber\\
\mu_i | X_i &\sim& N\left( \frac{ \sigma^2 Y_{\bullet w} + \tau^2 X_i}{Var(X_i)}, \frac{\sigma^2\tau^2}{Var(X_i)} \right) \nonumber 
\end{eqnarray}

Finally, Bayesian hypothesis testing can be performed. An arbitrarily selected significance level of $\alpha =0.05$ allows the 0.05 and the 0.95  quantiles of the posterior distribution to be used as appropriate thresholds. These quantiles were calculated, for each $X_i$, by using the \texttt{qnorm} method, which is given a probability ($\alpha$), the prior mean ($Y_{\bullet w}$) and the standard deviation ($\sqrt{\tau^2}$) and returns a number whose cumulative distribution matches the probability.  This prior distribution value is, along with the posterior mean and standard deviation, given as parameters to the \texttt{pnorm} method. The results corresponds to $P(H_0 | X_i)$, $P(H_1 | X_i)$, and $P(H_2 | X_i)$ respectively. The prior upper quantile is used as value parameter of \texttt{pnorm} to find (\ref{ph1}), the prior lower quantile for (\ref{ph2}), and finally $P(H_2 | X_i)$ from the results of the other calculations as described in (\ref{ph0}).

\begin{eqnarray}
P(H_1 | X_i) &=& P(\mu_i - \theta_0 > \alpha \hspace*{0.2cm} | X_i ) \label{ph1}\\
P(H_2 | X_i) &=& P(\mu_i - \theta_0 < - \alpha \hspace*{0.2cm} | X_i ) \label{ph2}\\
P(H_0 | X_i) &=& P(|\mu_i - \theta_0|\leq\alpha \hspace*{0.2cm} | X_i )  = 1 - P(H_0 | X_i) - P(H_2 | X_i)\label{ph0}
\end{eqnarray}

The false discovery rates can be calculated from the values, which were found in (\ref{ph1}) - (\ref{ph0}). This can be done using any technique that produces the rates from p-values. In this implementation the package \texttt{fdrtool} and its method \texttt{censored.fit} were used to calculate the rates.















\subsection{Similar projects}
There are other projects, which were considered to be implemented as either extensions to the existing RNAice project, and as an alternative to the Bayesian hit selection method that was implemented.

\subsubsection{RNAither, \citeauthor{Rieber2009}}
RNAither, is an R-package that performs analysis of high-throughput RNA interference knock-down experiments. It generates lists of relevant genes and pathways from raw experimental data. It contains a range of options for data normalization, and statistical tests, along with significance analysis. This packages also contains an output and wrapper functions that represent a typical work flow. RNAither is short of a complete solution, because the target users of the RNAice project are the researchers who might not have enough experience with R, whereas RNAither is a toolbox of methods to be used in R. 

\subsubsection{Web cellHTS2, \citeauthor{Pelz2010}}
Web cellHTS2\footnote{\url{http://web-cellHTS2.dkfz.de/}} is a  web-application for HTS analyses. It provides a step-by-step configuration for the analysis of single and multi-channel experiments. The provides an extensive HTML reports for the user containing all the results of the analyses. It is implemented in Java, where it call a backend R-server implementation to perform the calculations. At the time of writing, the web application was running, but the author was unable to use it, as said none of the browser tried was supported.

\subsubsection{HTSanalyzeR, \citeauthor{Wang2011}}
HTSanalyzeR is a an R-package that takes HTS data input, which already undergone preprocessing and quality control. The package can perform gene sets analysis or network enrichment methods to perform the hit analyses. The output is in the form of HTML, network figures and enrichment maps. It has a special pipeline for cellHTS2 objects and has beenbuilt into the web-cellHTS interface. This package has the potential to be an extension to RNAice, since an conversion to its input format is a possibility. It might further enhance the analysis strength of the project, however extending RNAice with this package is outside the scope of this project.











