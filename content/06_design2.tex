
\subsection{Conceptual overview of SAVANAH}
As mentioned in section \ref{intellij}, SAVANAH was created using the Spring MVC framework, with Hibernate and Groovy/Grails. The project SAVANAH itself actually utilizes another project called HtsBackend which will be explained in section \ref{htsbackend}. The intended functionality of SAVANAH, once finished, is to enable users to upload test results, and analyse the data, as well as storage of said test results for later use or analysis.


\begin{figure}[H]
\centerline{\includegraphics[width=0.5\linewidth]{img/savanah-uml.png}}
\caption{\scriptsize SAVANAH and HtsBackend UML diagram (large scale on Figure \ref{fig:savhtsuml-big})}
\label{fig:savhtsuml}
\end{figure}

\subsubsection{Role of HtsBackend}\label{htsbackend}
The project HtsBackend contains all of the \textit{Model}s used to describe the problem domain of RNA analysis, what kind they were, and the results. This means that HtsBackend is the actual core of the project, whereas SAVANAH is used as a mediator for manipulating the underlying data structure in HtsBackend. Even though HtsBackend is used as a data structure foundation, it also provides \textit{Controller}s and \textit{View}s for the user, so they are able to see the \textit{Model}s individually. More importantly, HtsBackend allows to share database and functionality across several grails tools used for working on high-throughput screening data.



\subsubsection{Role of Controller - LibraryFileUpload}
This \textit{Controller} is the wrapper for the functionality provided in the \textit{LibraryUploadService}, described in section \ref{sav-libup}. It contains two actions, an \texttt{index}, which has a \textit{View} where a web form is placed that the user needs to fill out in order to upload the file. It also has the action \texttt{upload}, which is being called when the user filled out the before mentioned form with a comma separated file. The text file it accepts needs to have the correct format, otherwise nothing will be uploaded.


\subsubsection{Role of Service - LibraryUpload}\label{sav-libup}
This \textit{Service} is called by the \textit{LibraryFileUploadController} to handle the upload of the library files. It basically checks whether the file has all the valid fields, and iterates over all of them. For simplicity, the method is described as pseudo-code below:

\begin{enumerate}

\item Initiation of new \textit{Library} object and other variables.

\item foreach \textit{line} in the library file:
	\begin{enumerate}

	\item If it is the first \textit{line}, then it contains the header, so find all columns and save their corresponding index to a variable.
	\item If any of the index variables of the headers is not set, return error.
	
	\item Create a new \textit{Entry} object and check if the plate index found on the line is in the database. If so, use it, otherwise create a new \textit{LibraryPlate} object. Then fill it out.

	\item Check for the naming convention to use for the sample name, making it the miRBase.ID.miRPlus.ID if it only contains one element, otherwise name it with "X-" concatenated with the last ciphers of the miRBase ID. 
	
	\item Create a new \textit{Sample} if if it does not exist. If it does exist, use it. 
	
	\item For each of the miRBase ID's, create the \textit{Identifier} object and place it in the sample.
	
	\item Add the new \textit{Entry} to the \textit{Library}.
	
	\end{enumerate}
	
	\item Save all new objects to the database.
\end{enumerate}


\subsubsection{Role of Controller - LibraryToExperiment}

Once a library has been created, the user needs to be able to create an experiment from this library. The \textit{LibraryToExperimentController} provides the user with this functionality. The user can set the following parts, necessary to create the experiment:

\begin{itemize}
\item Experiment name and date.
\item Select the project it belongs to, which is a mandatory field.
\item A field for specifying the number of replicate numbers as a range.
\item A field for specifying the lowest number of the replicates.
\item The default cell-line.
\item A user-specified pattern to generate a barcode for the \textit{Plate} object. This pattern should include "\textbackslash \textbackslash R" and "\textbackslash \textbackslash P", i.e. "E\textbackslash \textbackslash PM1D\textbackslash \textbackslash R". "\textbackslash \textbackslash R" will be replaced with the replicate number, and "\textbackslash \textbackslash P" with the two digit plate number.
\item The name of the \textit{Plate} layout, which should also contain a "\textbackslash \textbackslash P" as with the barcode pattern.
\end{itemize}

All this input is then parsed and sent within a transaction to the \textit{LibraryToExperimentService}. Wrapping this operation inside a transaction gives the actions inside it atomicity, either they all happen, or none do. 


\subsubsection{Role of Service - LibraryToExperiment}
Once \textit{LibraryToExperimentController} calls \texttt{create} on \textit{LibraryToExperimentService}, the service first validates all of the fields, checking whether they have been correctly filled out, and throws an error if they were not. Afterwards, there is created a new \textit{Experiment} object, and for each \textit{LibraryPlate} within the \textit{Library} selected by the controller there is created a new \textit{PlateLayout}. For each \textit{PlateLayout}, there is added replicate \textit{Plate}s with the specificed barcode, and is referenced to the \textit{Library}. Once this has been completed, a \textit{PlateLayoutService} (not a part of the extensions made in this project) is called to create all the \textit{WellLayouts} of the experiment.




\subsubsection{Role of Service - Unzip}\label{sav-unzip}
This service is an exact copy of the functionality of the \textit{UnzipService} from the  GeneInteractions, described in section \ref{gi-unzip}. In this project, however, the service is actually put to use. When parsing a batch of excel-file readouts, it is necessary to unpack the files from the .zip-file in which they were compressed. This is the exact functionality of the \textit{UnzipService}, only generalized to unzip any type of file to be contained within.




\subsubsection{Role of Service - XlsxToReadout}\label{xlsxToReadout}
The \textit{XlsxToReadoutService}-method \texttt{parseToReadout} converts a data sheet to a \textit{Readout}, adding it to a \textit{Plate} based on its file name. For each line in the excel file, it converts the data into a new \textit{WellReadout}, which references the \textit{Readout}. \texttt{parseToReadout} has an overloaded method which takes a \textit{MultipartFile} as input, instead of an Excel-file. This is to enable multiple sources for the data file, \textit{MultipartFile} being the default upload file-type in Grails. In this overloaded method, the file is first converted using the \textit{UnzipService} and in turn calls the other \texttt{parseToReadout}, which takes an excel-file. 


\subsection{R Project - Bayesian RNAi Hit Selection}
The Bayesian hit selection algorithm explained in Section \ref{bayesian}, was implemented as an R-script method, meaning the method can be called from any code that have it as source. The implementation follows the description from Section \ref{bayesian} to the letter, where it utilizes the package \texttt{fdrtool}, which allows for estimation of false discovery rates based on p-values. The main method is called \texttt{bayesianHitSelection}, and takes three arguments; a relative path to the input file, an activator cutoff value, and an inhibitor cutoff value.
Once the prior and posterior mean and variance has been found, the sub method \texttt{bayesianHypothesisTesting} is called. This, as the name implies, performs the hypothesis testing, and returns p-values for all three hypotheses. 