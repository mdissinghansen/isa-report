\newpage
\newsection{Choice of technology}
%Identify candidate technologies and describe their key features briefly.

%Describe the experiments you have performed as part of making your technology choice, if experiments were necessary to make a qualified technology choice!

%Describe the considerations and evaluation criteria for your choice. Do you consider the technology to be mature for real-life deployment?



\subsection{IntelliJ IDEA}\label{intellij}
IntelliJ IDEA is a development platform for creating projects in Java or Java-derivative languages. It supports the Spring MVC framework, as well as a full-fledged local testing environment, without the need for an actual database. The latter feature really improves testability, as one is unable to produce errors in any deployed database, and increases the development speed. 

The development platform for SAVANAH was chosen when the author started working on extensions, so it was natural to continue the use of this platform, as it filled the requirements set at the start of its project, regarding persistence through Hibernate, section \ref{hibernate}, and modelling through Groovy and Grails, section \ref{groovy}.



\subsubsection{Hibernate}\label{hibernate}
Hibernate is a so called Object/Relational Mapping (ORM), framework for Java. The purpose of Hibernate is to enable developers to easily persist data, without having to write any SQL code. Hibernate is a suitable solution for persistence, as it is easy to learn, is highly reliable, and robust.


\subsubsection{Groovy/Grails}\label{groovy}
Groovy is a dynamic language for the Java Virtual Machine (JVM). It resembles a simplified version of Java, and one is able to use Java code alongside Groovy code. Grails is an open source web application framework for the Java Virtual Machine. Grails utilizes the Groovy programming language to create an agile web framework to make it suitable for customizations. 


\subsubsection{MVC - Model View Controller}
The MVC framework is the foundation on which SAVANAH was created. This is a software pattern for implementing user interfaces. It divides the responsibilities of the application into three classes (Figure \ref{fig:mvc}). 
A \textit{View} is usually a definition of some output from a controller to a user. This could be a web page (or GUI) that allows the user to interact with a \textit{Controller} to change a \textit{Model}. The \textit{Model}  defines the object that can be manipulated through the business logic implemented in a \textit{Controller}.

As an example, consider the GeneInteractions project, which is also based on the MVC framework, where an example \textit{Model} would be a \textit{Gene}. To manipulate the \textit{Gene}, this functionality has to be provided through a \textit{Controller}. The user then interacts with the  corresponding \textit{View}, which is usually a web page in this project, and can thereby manage the \textit{Gene} through this interface.

An addition to the MVC framework is the implementations of \textit{Services}. \textit{Services} are a crucial part of the framework, as they will be the intermediate layer between \textit{Controller}s and the \textit{Models}. As a result, a \textit{Service} contains the logic to get or update models, or other requests. Thus all manipulation of models, should be done through a method-call to a \textit{Service}.

This description of the MVC framework, applies to the web based Spring MVC framework, and there are other variations of this framework that are in use on other development platforms.


%You don't mention service classes here. They are a crucial part of grails and extend the MVC pattern such that the controller is merely the mediator of model and view, where all business logic is delivered by services. Even more elegant than MCV alone!

\begin{figure}[!h]
\centerline{\includegraphics[width=11cm]{img/mvc.png}}
\caption{\scriptsize Image of how an Model View Controller.}
\label{fig:mvc}
\end{figure}
\newpage

\subsection{RStudio}
RStudio is an Integrated Development  Environment (IDE) for running and creating R related projects. This IDE also supports Shiny, section \ref{shinySec}, very intuitively. It was used for creating all of the R projects mentioned in this report because the author had experience with the IDE and supported all of the needed functionality.


\subsection{R "Shiny"-package}\label{shinySec}
Shiny\footnote{\label{shiny}\url{http://www.rstudio.com/shiny/} October 5. 2013} is an R-package from RStudio which makes it easy to create interactive web applications, based on R. This means that one does not need to know about HTML/Javascript or any server-side language, as long as the developer knows R. Shiny is easy to use, and initially it consists of only two main files. 

\subsubsection{ui.R}
The first file is called "ui.R", and it contains the logic needed to create the client side Html/Javascript. The code snippet below is taken from the VennDiagram project that was also created as a part of this project. It shows the basic buildup of the client part. First it defines a \texttt{shinyUI} method. The parameters of this method then defines a web page. And this page consist of a comma-separated list of elements, defined by their functions. As can be seen on the code, there is an element called \texttt{selectInput} which correspond to a Html select-box (dropdown). When wanting to show some output, defined in the "server.R" file, i.e. it is shown as an \texttt{plotOutput}, which converts into an Html img-element.

\begin{lstlisting}[language=R]
shinyUI(pageWithSidebar(
...
	selectInput('nrSet','Select nr. of sets', seq(0, 5, 1)),
...
	plotOutput(outputId = "vennplot")
...
\end{lstlisting}      
      
      
\subsubsection{server.R}
In much the same way as the "ui.R" file, the "server.R" file defines what should be done on the server side. Its base consist of a method called "shinyServer", which then takes some parameters that can be used on the server side. The input-parameter is the set of values from the input elements defined in the "ui.R" file. The output-parameter is then the output of what we want to do, the handling/parsing/analysis of the data. It is defined in such a way that one can easily output a table or a plot, by using \texttt{renderPlot}- or \texttt{renderTable}-method. The \texttt{renderUI} is more complex and will be described in the chapter with the VennDiagram project.

\begin{lstlisting}[language=R]
shinyServer(function(input, output, clientData) { 
  output$selectUI <- renderUI(expr = {   
...  
  inputVals <- reactive({
...  
  output$values <- renderTable({
...  
  output$vennplot <- renderPlot({
\end{lstlisting}

In the sample code above, there is also the tag \texttt{reactive}, which is used to define a reactive dataset. The idea behind this is that each expression will only update if an input parameter it depends on changes.. This way, when the input data becomes invalid, it can be updated automatically, such that all reactive expressions that directly or indirectly depend on that value can be re-executed. 


\subsection{Alternatives to Shiny}
During the course of the project, different ways for SAVANAH to talk to an R application were discussed, since it would be preferable to keep some calculations in R, as they have proven robust and correct. Additionally, it is possible to grasp back on a large number of implemented algorithms. Newly published algorithms are also usually available as R code and can be quickly included into the framework.  The idea was to create an R-service which could take a web request (post or get) from SANAVAH, and either return the result to SAVANAH or save it in a database where SAVANAH could also retrieve the results. The two ways for R to connect to SAVANAH were investigated by utilizing the Rook- and RCurl-packages.

\subsubsection{Rook-Package}
In order to investigate the usability of Rook as a substitute for Shiny as a web framework for R, there was implemented as prototype which can be found in the git repository\footnote{\url{https://bitbucket.org/mdissinghansen/isa-r-code/} in the folder "Rook".}. The prototype made with this package yielded the result that it is possible to host a web service build in R. In comparison to Shiny, Rook is more of a web server interface, where Shiny can do both user interface as well as web server interface, with little work needed from the programmer.

\subsubsection{RCurl-package}
The RCurl attempts to communicate with a Grails application was taken further with this package. The resulting can also be found in the git repository\footnote{\url{https://bitbucket.org/mdissinghansen/isa-r-code/} in the folder "HttpPostToGrails".}, The RCurl package method \texttt{postForm} takes the input parameters: url, JSON packaged values and result. An important note regarding the usage of RCurl is that "id" and "value" are reserved variables and will not be parsed by a Grails application. The Grails application GeneInteraction\footnote{\url{https://bitbucket.org/mdissinghansen/isa-geneinteractions}} is the project that this code communicates with, in the controller \textit{RResult} and action \textit{saveResult}.


%Identify candidate technologies and describe their key features briefly.

%Describe the experiments you have performed as part of making your technology choice, if experiments were necessary to make a qualified technology choice!

%Describe the considerations and evaluation criteria for your choice. Do you consider the technology to be mature for real-life deployment?