\newpage
\newsection{System design and implementation}
\subsection{Overview of GeneInteractions}
%Describe the different elements of the system and how they are connected; the architecture of the system e.g., client-server, MVC, etc.
The GeneInteractions system is a sample project that displays the different techniques that are to be implemented into the SAVANAH project. Like SAVANAH itself, this project was created as a IntelliJ IDEA Groovy/Grails project. This minimizes the amount of refactoring needed for the solutions to work in SAVANAH as well. As this is an MVC-project (Model-View-Controller), the parts will be explained separately.

\subsubsection{Role of Models (Gene, GeneInteraction, Genome)}
%Describe the role of each system element, including the high-level features it is a part of.
The models are Groovy-classes, meant to represent abstractions of the domain objects that we want to handle. In this case they are collections of genomes, genes and gene interactions. In this simplified model of the genes' domain, a \textit{Gene} has only a name and an id. A \textit{GeneInteraction} contains an id, a weight of the interaction, and ids of the two genes interacting. Lastly, we have the \textit{Genome}, which also has an id and a name, but also a set of \textit{Gene}-objects and a set of \textit{GeneInteraction}-objects. 
These models are used to test the chosen interaction-based visualization techniques, e.g. graphs, that are possible for implementation into SAVANAH.

\subsubsection{Role of Controller/View - JSON}
%Describe the role of each system element, including the high-level features it is a part of.

JSON is an open standard format, which uses human readable text to transmit data. The data is either an object, or set of objects consisting of attribute-value pairs. It is used primarily in the web environment, as an alternative to XML. As the name implies, JSON derive from the JavaScript scripting language. XML is not as common practice when transmitting data, and is more used for defining documents and configurations. Below is a comparison of how the same object is defined in JSON and XML:

\begin{lstlisting}
// XML
<quiz>
	<question>What is the meaning of life?</question>
	<answer>42.</answer>
</quiz>

// JSON
{"quiz": {
    "question": "What is the meaning of life?",
    "answer": "42."
}}

\end{lstlisting}

To ease the conversion from groovy-class object to JSON, a so-called JSON object marshaller was used . This was achieved by adding the following piece of code in the "grails-app/conf/BootStrap.groovy" file.

\begin{lstlisting}
// Registering a groovy class as a JSON object
JSON.registerObjectMarshaller(Gene){
    def output = [:]
    output["id"] = it.uid.toString()
    output["name"] = it.name
    return output
}
\end{lstlisting}

Once registering is completed, it is possible to convert the object to JSON format by simply writing "as JSON" after the variable in a return statement. The example below is from the JSONController/GenomeAsJSON method:

\begin{lstlisting}
def GenomeAsJSON(int id) {
    def genome = Genome.get(id)
    if(genome != null){
        render genome as JSON
    }else{
        render(status: 503, text: "Failed to retrieve genome.")
    }
}
\end{lstlisting}

Thus, as can be seen in the code snippet above, the return statement needed to return a JSON object is just "render {object} as JSON" (in this example "genome" is the object).


\subsubsection{Role of Controller/View - Cytoscape}
%Describe the role of each system element, including the high-level features it is a part of.
One of the visualization libraries that could be implemented into SAVANAH is the Cytoscape.js plugin. This JavaScript library shares only name with the end-user program Cytoscape, but is not the same. This JavaScript adaptation was created for web programmers. It provides a reusable graph widget, which can be easily integrated into a web application. This project uses version 2.0.2 and was downloaded from the project website\footnote{\url{http://cytoscape.github.io/cytoscape.js/},  October 5. 2013}.

\begin{lstlisting}
function onclick(){
	// Asynchronous call to json-controller
	getJSON(<json-controller-url>, cytoscapeCallBackWithData);
}

// Significantly simplified to show basic functionality
function cytoscapeCallBackWithData(data){
	nodeStack = data.genes;
	edgeStack = data.geneInteractions;
	
	// cy is a <div> html-element
	cy.cytoscape({elements:{nodes=nodeStack, edges=edgeStack}});
}
\end{lstlisting}

The usage is illustrated as pseudo javascript/jQuery-code above. The cytoscape library was integrated by having a jQuery-click function, which was attached to a button. This function then used the jQuery method \texttt{getJSON}, which in turn calls the \textit{JSONController}'s action GenomeAsJSON. The return data was then given to a JavaScript method called \texttt{cytoscapeCallBackWithData}.

In the \texttt{cytoscapeCallBackWithData}, the JSON \textit{Genome} data is then converted into a node (\textit{Gene}) stack and an edge (\textit{GeneInteraction}) stack. Once this is done, the Cytoscape-library's own \texttt{cytoscape}-method was called, which initializes the style and elements, and creates the resulting network graph.


\subsubsection{Role of Controller/View - D3/Dimple}
Another visualization library is the D3/dimple\footnote{\url{http://d3js.org/}, October 5. 2013} library, which is as well capable of producing a network graph, which is needed to draw gene interactions. It is used in much the same way as the Cytoscape library, and the pseudo-code for the integration would look almost identical. Its integration also uses the onclick-function and asynchronous call, which in turn calls a function called \texttt{drawD3Graph}. This function does the same conversion of the json-data as the \texttt{cytoscapeCallBackWithData}, and then applies force to the graph to make it fan out.

\subsubsection{Role of Controller/Service - Unzip}\label{gi-unzip}
A way to upload multiple compressed files would really aid the user in their creation and handling of experiment plates and readouts in the SAVANAH project. The experiment readouts would be compressed into one .zip-file, and it would significantly reduce the workload of the user, if it was possible to unpack and parse these files. The \textit{UnzipService} takes care of the former.

After uploading a .zip file, a \textit{Controller} will call the \texttt{unpack}-function on the \textit{UnzipService}-class. The following is a simplified pseudo-code version of what happens in the \texttt{unpack}-function:

\begin{lstlisting}
// Main method for UnzipService
unpack(zipFile) {
	def	unpackedList = new HashMap<string, File>();
	for(file in zipFile.entries){
		unpackedList.put(file.name, file);
	}
	return unpackedList;
}
\end{lstlisting}

What differs between the pseudo-code and implementation is that in addition to validation checks, a conversion from \textit{ZipEntry}-class to \textit{File}-class occurs. This is done through a function called \texttt{getFileFromStream}, which takes an InputStream and a file suffix as parameters. This method then creates a temporary file on the server, and returns it. The \texttt{unpack}-function also takes an InputStream as input and uses this method to create the zipFile.


\subsubsection{Role of Service - Xlsx (Excel)}
Since the \textit{UnzipService} takes care of the unpacking the experiment readouts, the \textit{XlsxService} takes care of the parsing of the readouts contained within the .zip-file. It does so, by utilizing a Grails-plugin called \texttt{excel-import:1.0.0}\footnote{Depends on \texttt{joda-time} plugin}. Since GeneInteractions only serves as a framework for testing functionality, it only handles the simple parsing of excel-files. Consequently it only shows how to get the correct sheet, and correct row/column values. The actual parsing to a readout will be explained in subsection \ref{xlsxToReadout}.


\subsection{Overview of RNAice}

The RNAice (RNA interference comprehensive evaluation) project is an R-Shiny application, which provides analysis methods for the users, who can utilize a wide range of tools. The Shiny-package is explained in section \ref{shinySec}. 

When the biologist researchers are doing interaction analyses, it is easier for the researcher to interpret results, if they are available in some visual form. Additionally, for further testing of the resulting graph in Cytoscape. As a result thereof, the authors contribution to this project consists of a method for creating an interaction graph, and an export functionality from the interaction graph to a Cytoscape-compatible .SIF format.\\

\subsubsection{Role of Interaction Graph and .SIF export}
The important part of this contribution comes from the reactive dataset "\textit{interaction.data}". This is a reactive variable containing the latest set table of gene's that fits the condition that it connects to $\geq$ "\textit{group.miRNAs.threshold}" number of miRNA's. "\textit{group.miRNAs.threshold}" is an input element (slider), which can be controlled from the client side. The method for finding the interactions is arbitrary, as it just runs through the list of possible targets, and increments the interactions that were found for each gene. The interesting part is that this was done using hashsets. Hashsets read/write operations only take $O(1)$ time, making this method very fast as it does not have to iterate over the entire array each time, to do this incrementation. The information saved in the resulting object is a matrix consisting of from's and to's of the gene interactions. The flow of the Shiny code for this, is shown on Figure \ref{fig:idf}.

\begin{figure}[!h]
\centerline{\includegraphics[width=1\linewidth]{img/InteractionDataFigure.png}}
\caption{\scriptsize Interaction data flow}
\label{fig:idf}
\end{figure}

\subsubsection*{Interaction Graph}
The interaction graph itself (Figure \ref{fig:ig}) uses the \texttt{renderPlot} to output the plot resulting from calling the \texttt{qgraph} method on the matrix defined in "\textit{interaction.data}". \\

\begin{figure}[!h]
\centerline{\includegraphics[width=1\linewidth]{img/interactionGraph.png}}
\caption{\scriptsize Interaction graph}
\label{fig:ig}
\end{figure}

\subsubsection*{Export to SIF}
Similarly the export-to-SIF button is using the \texttt{downloadHandler} method, which takes a filename and a content. The data being written to the content is just a concatenation of each matrix row from "\textit{interaction.data}" with the string " pp " in the middle to define what kind of interaction it is. This " pp " can be easily changed, either directly in the server.R, or by creating a new ui.R element that defines what the interaction should be. An example of this output file is shown below:\\

\begin{lstlisting}
AP3M1 pp miR-183
AP3M1 pp miR-204
ARPP21 pp miR-133b
\end{lstlisting}


\subsection{Overview of VennDiagram}
The VennDiagram project\footnote{Can be found on \url{https://bitbucket.org/mdissinghansen/isa-r-code/}} is an Shiny application utilizing the same package as explained in section \ref{shinySec}. This project provides the user with the ability to create a venn diagram consisting of 1 to 5 sets, defined by the user. 
The process of parsing the comma-separated list of genes for each set is arbitrary, as it just added to the set of resulting genes. After this is done, the matrix for creating the venn diagram is builded. To generate the venn diagram, the \texttt{VennDiagram} method is called from the VennDiagram-package.

\subsubsection{Role of Dynamic generation of UI elements}
When creating the venn diagram, it was a constraint to only generate the amount of sets that should be usable. This resulted in the creation of a method that can dynamically generate an arbitrary number of elements. The result from this method is then given to the built-in method called \texttt{renderUI}, which renders the output as if it was defined in ui.R:

\begin{lstlisting}[language=R]
shinyServer(function(input, output, clientData) { 
  
  # New dynamic generation of elements, based on the input "nrSet".
  output$selectUI <- renderUI(expr = {  
...
\end{lstlisting}

And then in the ui.R, the output is defined as follows:

\begin{lstlisting}[language=R]

shinyUI(pageWithSidebar(
  headerPanel("Venn Diagram"),
  sidebarPanel(
  selectInput('nrSet','Select nr. of sets', seq(0, 5, 1)),
  p(strong("[Sets of genes must be comma ',' separated]")),
  div(htmlOutput("selectUI")) 
...  
\end{lstlisting}

The code above shows how the output of the dynamic method \texttt{renderUI} is parsed directly as Html, and placed in a Html div element, by using the \texttt{htmlOutput} method. The only downside to creating the dynamic inputs this way is, that if the user decides he/she wants an additional set, then the values, of the already created sets, will be reset.

